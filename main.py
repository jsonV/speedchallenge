import os
import sys
import cv2
import time
import pathlib
import pandas as pd
from PIL import Image
import saveVideoFrames
import tensorflow as tf
from prepareData import createData
from model import build_cnn_model
from partitionData import assignData, getSpeed
from opticalFlow import opticalFlowDense
from tqdm import tqdm
import numpy as np
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint

BATCH_SIZE = 32 
EPOCHS = 20 
WIDTH, HEIGHT, CHANNELS = 220, 68, 3
AUTOTUNE = tf.data.experimental.AUTOTUNE

def read_image_from_path(image_path):
    img = cv2.imread(image_path).astype(np.float32)/255.0
    return img

def pull_sample(dataSet, batch_size=BATCH_SIZE):
    image_batch = np.zeros((batch_size, HEIGHT, WIDTH, CHANNELS))
    label_batch = np.zeros((batch_size))
    while True:
        for i in range(batch_size):
            index = np.random.randint(len(dataSet))
            frame = dataSet.iloc[[index]].reset_index()
            x1, y1 = read_image_from_path(frame['Path'].values[0]), frame['Speed'].values[0]
            image_batch[i] = x1 
            label_batch[i] = y1
        yield (image_batch, label_batch)

def learnFromVideo(video = "data/train.mp4", saveFrame = False):
    print("Starting challenge script")
    # Randomize video into training/validation/test sets (60/20/20) split
    frameDir="frames/"
    trainDir="trainOF/"
    validDir="validOF/"
    testDir="testOF/"
    (trainImg, validImg, testImg) = createData(frameDir, trainDir, validDir, testDir, False, False, False, False)
    trainLabels = getSpeed(trainImg) # Returns (frame number, speed)
    validLabels = getSpeed(validImg) # Returns (frame number, speed)

    # Feed training set into neural network
    train_data = pd.DataFrame(data = trainLabels, columns = ['Path', 'Speed'])
    valid_data = pd.DataFrame(data = validLabels, columns = ['Path', 'Speed'])
    train_data['Path'] = trainDir + train_data['Path'].astype(int).astype(str) + '.jpg'
    valid_data['Path'] = validDir + valid_data['Path'].astype(int).astype(str) + '.jpg'
    train_generator = pull_sample(train_data)
    valid_generator = pull_sample(valid_data)
    val_size = len(valid_data.index)
    print("Loading Dataset into tf.Dataset...")
    model = build_cnn_model()
    mse = tf.keras.losses.MeanSquaredError()
    model.compile(optimizer=tf.keras.optimizers.Adam(),
                    loss=mse,
                    metrics=['accuracy'])
    modelName = "cnn-{}".format(int(time.time()))
    tb = tf.keras.callbacks.TensorBoard('logs/{}'.format(modelName))
    es = EarlyStopping(monitor='val_loss', patience=3, min_delta=1e-2, verbose=1)
    checkpoint_path = "training/cp-{epoch:04d}.ckpt"
    checkpoint_dir = os.path.dirname(checkpoint_path)
    cp = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
        verbose=1,
        save_weights_only=True)
    callbacks = [tb, es, cp]
    history = model.fit(train_generator, steps_per_epoch=400, epochs=EPOCHS, callbacks=callbacks, verbose=1, validation_data=valid_generator, validation_steps=5)

def testModel():
    testDir="testOF/"
    testImg = [int(file.split('.')[0]) for file in os.listdir(testDir)]
    testLabels = getSpeed(testImg) # Returns (frame number, speed)
    test_data = pd.DataFrame(data = testLabels, columns = ['Path', 'Speed'])
    test_data['Path'] = testDir + test_data['Path'].astype(int).astype(str) + '.jpg'
    testGenerator = pull_sample(test_data)

    model = build_cnn_model()
    mse = tf.keras.losses.MeanSquaredError()
    model.compile(optimizer=tf.keras.optimizers.Adam(),
                    loss=mse,
                    metrics=['accuracy'])
    model.load_weights('training/cp-0010.ckpt')
    tesw = next(testGenerator)
    print(tesw)
    prediction = model.predict(tesw[0])
    print(prediction) 

def submission():
    def pullFrames(dataSet, batch_size=BATCH_SIZE):
        image_batch = np.zeros((batch_size, HEIGHT, WIDTH, CHANNELS))
        label_batch = np.zeros((batch_size))
        count = 0
        index = 0
        while count*batch_size + index < 128:
            for i in range(batch_size):
                index = i
                frame = dataSet.iloc[[count*batch_size + i]].reset_index()
                x1, y1 = read_image_from_path(frame['Path'].values[0]), frame['Speed'].values[0]
                image_batch[i] = x1 
                label_batch[i] = y1
            yield (image_batch, label_batch)
    cap = cv2.VideoCapture('data/test.mp4')
    if not cap.isOpened():
        exit
    numFrames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    CROP_H = (210,150)
    CROP_W = (100,100)
    resize = (220, 68)
    model = build_cnn_model()
    mse = tf.keras.losses.MeanSquaredError()
    model.compile(optimizer=tf.keras.optimizers.Adam(),
                    loss=mse,
                    metrics=['accuracy'])
    model.load_weights('training/cp-0010.ckpt')
    test_speeds = []
    ret, frame_prev = cap.read()
    ret, frame_curr = cap.read()
    frame_prev = frame_prev[ CROP_H[0]:-CROP_H[1], CROP_W[0]:-CROP_W[1] ]
    frame_prev = cv2.resize(frame_prev, resize, cv2.INTER_AREA)
    frame_curr = frame_curr[ CROP_H[0]:-CROP_H[1], CROP_W[0]:-CROP_W[1] ]
    frame_curr = cv2.resize(frame_curr, resize, cv2.INTER_AREA)
    with open('test.txt', 'w+')  as file:
        while(cap.isOpened()):
            ret, frame_next = cap.read()
            if not ret:
                break
            frame_next = frame_next[ CROP_H[0]:-CROP_H[1], CROP_W[0]:-CROP_W[1] ]
            frame_next = cv2.resize(frame_next, resize, cv2.INTER_AREA)
            bgr = opticalFlowDense(None, image=[frame_prev, frame_curr, frame_next]).astype(np.float32)/255.
            bgr = (np.expand_dims(bgr,0))
            prediction = model.predict(bgr)
            test_speeds.append( prediction )
            frame_prev = frame_curr
            frame_curr = frame_next
            file.write(str(prediction[0][0])+ '\n')
def filterSubmission():
    with open('test.txt', 'r') as file:
        data = file.readlines()

    for i in range(len(data)):
        value = float(data[i].strip())
        if value < 0.0:
            data[i] = "{}\n".format(0.0)

    with open('test.txt', 'w') as file:
        file.writelines(data)

if __name__ == "__main__":
    #learnFromVideo()
    #submission()
    filterSubmission()
