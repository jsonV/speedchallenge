import cv2
import numpy as np

def opticalFlowDense(frame, inputDir="frames/", outputDir=None, image=None):
    """
    Calculates Farnback Optical Flow (OpenCV) angle/mag and converts it into BGR image.
    frame - integer representing frame number from corresponding inputDir.
    outputDir - 0, 1, or 2 to specify training/validation/testing directory, respectively.
    """
    if not image:
        filePrev = inputDir + str(frame-1)+".jpg"
        fileCurr = inputDir + str(frame)+".jpg"
        fileNext = inputDir + str(frame+1)+".jpg"
        image_prev = cv2.imread(filePrev)
        image_curr = cv2.imread(fileCurr)
        image_next = cv2.imread(fileNext)
    else: 
        image_prev = image[0]
        image_curr = image[1]
        image_next = image[2]
    prev = cv2.cvtColor(image_prev, cv2.COLOR_BGR2GRAY)
    curr = cv2.cvtColor(image_curr, cv2.COLOR_BGR2GRAY)
    nxt = cv2.cvtColor(image_next, cv2.COLOR_BGR2GRAY)
    
    hsv = np.zeros_like(image_curr)
    # set saturation
    hsv[...,1] = 255
 
    flow = None
    pyr_scale = 0.5
    levels = 1
    winsize = 15
    iterations = 2
    poly_n = 5
    poly_sigma = 1.3
    flags = 0
    of = cv2.calcOpticalFlowFarneback(prev,
                                      nxt,  
                                      flow, 
                                      pyr_scale, 
                                      levels, 
                                      winsize, 
                                      iterations, 
                                      poly_n, 
                                      poly_sigma, 
                                      flags)
                                        
    mag, ang = cv2.cartToPolar(of[..., 0], of[..., 1])  
    hsv[...,0] = ang * 180/ np.pi / 2
    hsv[...,2] = cv2.normalize(mag,None,0,255,cv2.NORM_MINMAX)
    bgr = cv2.cvtColor(hsv,cv2.COLOR_HSV2BGR)
    if outputDir != None:
        outDir = ["trainOF/","validOF/","testOF/"][outputDir]
        cv2.imwrite(outDir+str(frame)+".jpg", bgr)
    return bgr 