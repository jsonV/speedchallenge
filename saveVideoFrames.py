import cv2
from tqdm import tqdm

def saveVideoFrames(height, width, video=None, direc="frames/", resize=(220,68), write=False, show=False):
    '''
    Displays and/or saves video's frames into specified directory. May also 
    crop/scale the video frames before saving.

    height - represents top and bottom margins
    width - represents left and right margins
    resize - A tuple of the final output dimension of the image (width, height)
    '''
    if not video:
        cap = cv2.VideoCapture('data/train.mp4')
    else:
        cap = cv2.VideoCapture(video)
    count = 0
    length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frameWidth  = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frameHeight = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    for i in tqdm(range(length)):
        if not cap.isOpened():
            break
        success,image = cap.read()
        if success:
            image = image[ height[0]:-height[1], width[0]:-width[1] ]
            if resize:
                image = cv2.resize(image, resize, cv2.INTER_AREA)
            if write:
                cv2.imwrite(direc + "%d.jpg" % count, image)
            if show:
                cv2.imshow('images',image)
                if cv2.waitKey(50) & 0xFF == ord('q'):
                    break
            count += 1
    print("Counted", count, "frames. (0 indexed))")
    return count

if __name__ == "__main__":
    saveVideoFrames((210,150), (100,100), resize=(220, 68), write=True)