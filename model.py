import os
import gc 
import cv2
import random
import errno
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tqdm import tqdm
from partitionData import assignData
from opticalFlow import opticalFlowDense

assert len(tf.config.list_physical_devices('GPU')) > 0 # RUN ON GPU!

def build_cnn_model():
    cnn_model = tf.keras.Sequential([
        # 68, 220
        tf.keras.layers.Conv2D(filters=24, kernel_size=(5,5)),
        # 64, 216
        tf.keras.layers.MaxPool2D(pool_size=(2,2)),
        tf.keras.layers.ELU(),
        # 32, 108
        tf.keras.layers.Conv2D(filters=36, kernel_size=(5,5)),
        # 28, 104 
        tf.keras.layers.MaxPool2D(pool_size=(2,2)),
        # 14, 52
        tf.keras.layers.ELU(),
        tf.keras.layers.Conv2D(filters=48, kernel_size=(5,5)),
        # 10,48 
        tf.keras.layers.MaxPool2D(pool_size=(2,2)),
        # 5, 24 
        tf.keras.layers.ELU(),
        tf.keras.layers.Dropout(0.40),
        tf.keras.layers.Conv2D(filters=64, kernel_size=(3,3)),
        # 3, 22
        tf.keras.layers.Conv2D(filters=64, kernel_size=(3,3)),
        # 1, 20
        tf.keras.layers.ELU(),
        tf.keras.layers.Flatten(), # 1 * 20 * 64
        tf.keras.layers.ELU(),
        tf.keras.layers.Dense(100),
        tf.keras.layers.Dense(50),
        tf.keras.layers.Dense(1)
    ])
    return cnn_model

