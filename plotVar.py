from matplotlib import pyplot as plt

def plotVariable(file):
    '''Helper function to plot useful variables over time (e.g. Distance/Speed/Acceleration)'''
    with open(file) as file:
        lines = [float(line.rstrip('\n')) for line in file]
    plt.plot(lines)
    plt.show()
