import numpy as np

def calculateRMSE(targets, predictions):
	''' Takes in two numpy arrays and returns RMSE, arrays must be same length'''
	return np.sqrt(np.mean((predictions-targets)**2))

if __name__ == "__main__":
	print(calculateRMSE(np.array([1,1,1,1,1]), np.array([3,3,3,3,2])))