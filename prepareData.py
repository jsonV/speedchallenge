import os
import tensorflow as tf
import cv2
import errno
import numpy as np
from tqdm import tqdm
from saveVideoFrames import saveVideoFrames
from partitionData import assignData
from opticalFlow import opticalFlowDense

def createData(frameDir, trainDir, validDir, testDir, videoToFrames=False, createTrainFrames=False, createValidateFrames=False, createTestFrames=False, **kwargs):
    '''
    Creates training, validation and testing data sets out of original video and places them into there own respective directories.
    '''
    try:
        os.makedirs(frameDir, exist_ok=True)
        os.makedirs(trainDir, exist_ok=True)
        os.makedirs(validDir, exist_ok=True)
        os.makedirs(testDir, exist_ok=True)
    except FileExistsError:
        pass
    if videoToFrames:
        saveVideoFrames((210,150), (100,100), write=True)
    WIDTH, HEIGHT, CHANNELS = 220, 68, 3
    if createTestFrames or createTrainFrames or createValidateFrames:
        (trainImg, validImg, testImg) = assignData(frameDir)
        if createTrainFrames:
            print("Creating Optical Flow Training frames...")
            for frame in tqdm(trainImg):
                opticalFlowDense(frame, frameDir, 0)
        if createValidateFrames:
            print("Creating Optical Flow Validation frames...")
            for frame in tqdm(validImg):
                opticalFlowDense(frame, frameDir, 1)
        if createTestFrames:
            print("Creating Optical Flow Test frames...")
            for frame in tqdm(testImg):
                opticalFlowDense(frame, frameDir, 2)
    else:
        trainImg = [int(file.split('.')[0]) for file in os.listdir(trainDir)]
        validImg = [int(file.split('.')[0]) for file in os.listdir(validDir)]
        testImg = [int(file.split('.')[0]) for file in os.listdir(testDir)]
    return (trainImg, validImg, testImg)

if __name__ == '__main__':
    createData('frames/', 'trainOF/', 'validOF/', 'testOF/')
