from plotVar import plotVariable as ptv
FRAME_RATE = 20 # Global frame rate for footage is 20hz = 50 ms
TBF = .05 # Time between frames
with open("data/train-speed.txt") as file:
    lines = [float(line.rstrip('\n')) for line in file] 
with open("data/train-acceleration.txt", "w") as outfile:
    for i in range(1,len(lines)-1):
        acceleration = str((lines[i+1] - lines[i-1])/(2*TBF))
        outfile.write(acceleration + '\n')

ptv("data/train-speed.txt")
ptv("data/train-acceleration.txt")
