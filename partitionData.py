import os
import numpy as np
import random

def assignData(frameDir, seed=1337):
    ''' Paritions the full set of video frames into three sections for learning
    purposes. Data sets contain frame number. '''
    random.seed(seed) 
    count = len(os.listdir(frameDir))
    randomSample = random.sample(range(1,count-1), count-2) # Randomly orient time frames in video
    sixty = int(count*.6)
    eighty = int(count*.8)
    trainImg = [sample for sample in randomSample[0: sixty]]
    validImg = [sample for sample in randomSample[sixty: eighty]]
    testImg = [sample  for sample in randomSample[eighty: count]]
    return trainImg, validImg, testImg

def getSpeed(frames):
    ''' 
    Gets speed of list frames.
    '''
    length = len(frames)
    speeds = np.empty(shape=[length, 2])
    with open("data/train-speed.txt") as file:
        lines = [float(line.rstrip('\n')) for line in file]
    for count, frame in enumerate(frames):
        speeds[count][0] = frame
        speeds[count][1] = lines[frame]
    return speeds