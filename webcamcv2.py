import cv2

cap = cv2.VideoCapture(0);

while(True):
    ret, frame = cap.read()
    frame = frame[50:300, 50:300]
    cv2.imshow('frame', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
